print("x=")
phrase_string = input()
phrase_string = phrase_string.lower().replace(" ","")

len_str = len(phrase_string)
k_end = int(len_str/2)
is_palindrome = True
for k in {1,k_end}:
    if phrase_string[k-1] != phrase_string[len_str-k]:
        is_palindrome = False
    if is_palindrome == False:
        break

print(is_palindrome)