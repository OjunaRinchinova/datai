#import requests as req
import json
from bs4 import BeautifulSoup
import json
import tqdm
import time
data = {
    "data":[]
}
from requests_tor import RequestsTor
req = RequestsTor()

for page in range(0,5):
    url = f'https://ulan-ude.hh.ru/search/vacancy?search_field=name&search_field=company_name&search_field=description&text=python+%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA&from=suggest_post&page={page}&hhtmFrom=vacancy_search_list'
    #url = 'https://www.bsu.ru/university/general/'
    resp = req.get(url)
    #print(resp.text)
    soup = BeautifulSoup(resp.text, 'lxml')
    tags = soup.find_all(attrs={"data-qa":"serp-item__title"})
    for iter in tags:
        time.sleep(2)
        url_object = iter.attrs["href"]
        #print(iter.text)
        resp_object = req.get(url_object)

        soup_object = BeautifulSoup(resp_object.text, "lxml")
        tag_experience = soup_object.find(attrs={"data-qa":"vacancy-experience"}).text if (soup_object.find(attrs={"data-qa":"vacancy-experience"}) is not None) else ""
        #print(tag_experience)
        tag_salary = soup_object.find(attrs={"data-qa":"vacancy-salary"}).text if (soup_object.find(attrs={"data-qa":"vacancy-salary"}) is not None) else ""
        #print(tag_salary)
        tag_region = soup_object.find(attrs={"data-qa":"vacancy-serp__vacancy-address"}).text if (soup_object.find(attrs={"data-qa":"vacancy-serp__vacancy-address"}) is not None) else ""
        #print(tag_region)
        data["data"].append({"title": iter.text, "work experience":tag_experience, "salary": tag_salary, "region": tag_region})
        

        with open("data.json","w") as file:
            json.dump(data,file,ensure_ascii=False)
